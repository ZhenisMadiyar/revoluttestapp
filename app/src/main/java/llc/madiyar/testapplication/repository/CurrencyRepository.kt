package llc.madiyar.testapplication.repository

import io.reactivex.Observable
import llc.madiyar.testapplication.repository.api.CurrencyApi
import llc.madiyar.testapplication.repository.data.CurrencyResponse
import retrofit2.Response
import java.util.*

/**
 * Created by ZhenisMadiyar on 23,Февраль,2020
 */
class CurrencyRepository(private val currencyApi: CurrencyApi) {

    fun getCurrencies(base: String): Observable<Response<CurrencyResponse>> {
        return currencyApi.getCurrenciesList(base = base)
    }
}