package llc.madiyar.testapplication.repository.api

import io.reactivex.Observable
import llc.madiyar.testapplication.repository.data.CurrencyResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by ZhenisMadiyar on 23,Февраль,2020
 */
interface CurrencyApi {

    @GET("android/latest")
    fun getCurrenciesList(@Query("base") base: String): Observable<Response<CurrencyResponse>>
}