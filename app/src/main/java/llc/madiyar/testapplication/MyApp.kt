package llc.madiyar.testapplication

import android.app.Application
import android.os.Build
import llc.madiyar.testapplication.repository.CurrencyRepository
import llc.madiyar.testapplication.repository.api.CurrencyApi
import llc.madiyar.testapplication.viewmodel.CurrencyViewModel
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import timber.log.Timber

/**
 * Created by ZhenisMadiyar on 23,Февраль,2020
 */
class MyApp : Application() {

    //For the sake of simplicity, for now we use this instead of Dagger
    companion object {
        private lateinit var retrofit: Retrofit
        private lateinit var currencyApi: CurrencyApi
        private lateinit var currencyRepository: CurrencyRepository
        private lateinit var currencyViewModel: CurrencyViewModel

        fun injectCurrencyViewModel() = currencyViewModel
    }

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.uprootAll()
            Timber.plant(Timber.DebugTree())
        }
        retrofit = Retrofit.Builder()
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl("https://hiring.revolut.codes/api/")
            .build()

        currencyApi = retrofit.create(CurrencyApi::class.java)
        currencyRepository = CurrencyRepository(currencyApi)
        currencyViewModel = CurrencyViewModel(currencyRepository)
    }
}