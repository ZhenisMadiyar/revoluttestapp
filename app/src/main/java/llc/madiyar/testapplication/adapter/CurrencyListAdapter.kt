package llc.madiyar.testapplication.adapter

import android.content.Context
import android.text.Editable
import android.text.TextUtils
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_currency_rate_main.view.*
import kotlinx.android.synthetic.main.item_currency_rate_simple.view.*
import llc.madiyar.testapplication.R
import llc.madiyar.testapplication.repository.data.RateItem
import llc.madiyar.testapplication.viewmodel.data.roundOffDecimal
import java.util.*
import kotlin.collections.ArrayList
import kotlin.math.roundToInt

/**
 * Created by ZhenisMadiyar on 23,Февраль,2020
 */
class CurrencyListAdapter(private val context: Context) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var currencyList: ArrayList<RateItem> = ArrayList()

    lateinit var interfaceCommunicator: InterfaceCommunicator

    companion object {
        const val TYPE_MAIN = 0
        const val SIMPLE_OTHER = 1
    }

    fun setOnItemClickListener(interfaceCommunicator: InterfaceCommunicator) {
        this.interfaceCommunicator = interfaceCommunicator
    }

    interface InterfaceCommunicator {
        fun onItemSelected(item: RateItem)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        when (viewType) {
            TYPE_MAIN -> {
                val view =
                    LayoutInflater.from(context)
                        .inflate(R.layout.item_currency_rate_main, parent, false)
                return MainViewHolder(view)
            }
            SIMPLE_OTHER -> {
                val view =
                    LayoutInflater.from(context)
                        .inflate(R.layout.item_currency_rate_simple, parent, false)
                return SimpleViewHolder(view)
            }
            else -> {
                val view =
                    LayoutInflater.from(context)
                        .inflate(R.layout.item_currency_rate_simple, parent, false)
                return SimpleViewHolder(view)
            }
        }
    }

    override fun getItemViewType(position: Int): Int {
        return if (position == 0) TYPE_MAIN else SIMPLE_OTHER
    }

    fun loadList(mCurrencyList: List<RateItem>) {
        if (currencyList.isEmpty()) {
            currencyList.add(RateItem("EUR", 1.0))
            currencyList.addAll(mCurrencyList)
        } else {
            currencyList.forEach { oldItem ->
                mCurrencyList.forEach { newItem ->
                    if (currencyList.indexOf(oldItem) != 0)
                        if (oldItem.name == newItem.name) {
                            oldItem.value = newItem.value
                        }
                }
            }
        }
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return currencyList.size
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (holder) {
            is SimpleViewHolder -> {
                holder.bind(currencyList[position])
            }
            is MainViewHolder -> {
                holder.bind(currencyList[position])
            }
        }
    }

    fun changePosition(item: RateItem) {
        Collections.swap(currencyList, 0, currencyList.indexOf(item))
        notifyItemMoved(currencyList.indexOf(item), 0)
    }

    inner class SimpleViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(item: RateItem) {
            itemView.tv_currency_name.text = item.name
            itemView.tv_currency_value.text =
                (item.value * currencyList[0].value).roundOffDecimal().toString()
            itemView.setOnClickListener {
                interfaceCommunicator.onItemSelected(item)
            }
        }
    }

    inner class MainViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(item: RateItem) {
            itemView.tv_main_currency_name.text = item.name
            itemView.et_main_currency_value.setText(item.value.roundToInt().toString())
            itemView.et_main_currency_value.setSelection(itemView.et_main_currency_value.text.length)
            itemView.et_main_currency_value.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(p0: Editable?) {
                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    if (!TextUtils.isEmpty(p0)) {
                        item.value = p0.toString().toDouble()
                    }
                }
            })
        }
    }
}