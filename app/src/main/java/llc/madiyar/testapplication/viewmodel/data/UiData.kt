package llc.madiyar.testapplication.viewmodel.data

import llc.madiyar.testapplication.repository.data.RateItem

/**
 * Created by ZhenisMadiyar on 23,Февраль,2020
 */
data class CurrencyResult(val rateList: List<RateItem>, val message: String, val error: Throwable? = null)