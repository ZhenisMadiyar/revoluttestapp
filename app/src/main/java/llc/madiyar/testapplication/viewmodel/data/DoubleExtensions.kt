package llc.madiyar.testapplication.viewmodel.data

import java.math.RoundingMode
import java.text.DecimalFormat

/**
 * Created by ZhenisMadiyar on 24,Февраль,2020.
 */
fun Double.roundOffDecimal(): Double {
    return this.toBigDecimal().setScale(2, RoundingMode.UP).toDouble()
}