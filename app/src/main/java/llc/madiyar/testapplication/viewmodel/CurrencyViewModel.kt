package llc.madiyar.testapplication.viewmodel

import io.reactivex.Observable
import llc.madiyar.testapplication.repository.CurrencyRepository
import llc.madiyar.testapplication.repository.data.RateItem
import llc.madiyar.testapplication.viewmodel.data.CurrencyResult
import org.json.JSONObject

/**
 * Created by ZhenisMadiyar on 23,Февраль,2020
 */
class CurrencyViewModel(private val currencyRepository: CurrencyRepository) {

    fun getCurrencies(base: String): Observable<CurrencyResult> {
        return currencyRepository.getCurrencies(base = base)
            .map {
                val ratesJsonObject = JSONObject(it.body()?.rates?.toJson()!!)
                val ratesObjectKeys = ratesJsonObject.keys()
                val listRate = ArrayList<RateItem>()
                while (ratesObjectKeys.hasNext()) {
                    val currencyName = ratesObjectKeys.next()
                    val currencyValue = ratesJsonObject.get(currencyName)
                    listRate.add(
                        RateItem(
                            name = currencyName,
                            value = currencyValue as Double
                        )
                    )
                }
                CurrencyResult(listRate, "List of currencies")
            }
            .onErrorReturn {
                CurrencyResult(emptyList(), "An error accured", it)
            }
    }
}