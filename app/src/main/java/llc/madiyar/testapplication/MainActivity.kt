package llc.madiyar.testapplication

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import llc.madiyar.testapplication.view.CurrencyListFragment

/**
 * Created by ZhenisMadiyar on 23,Февраль,2020
 */
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null) {
            supportFragmentManager.beginTransaction()
                .replace(R.id.frag_container, CurrencyListFragment()).commit()
        }
    }
}
