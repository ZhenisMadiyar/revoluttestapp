package llc.madiyar.testapplication.view

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_currency_list.*
import llc.madiyar.testapplication.MyApp
import llc.madiyar.testapplication.R
import llc.madiyar.testapplication.adapter.CurrencyListAdapter
import llc.madiyar.testapplication.repository.data.RateItem
import llc.madiyar.testapplication.viewmodel.data.CurrencyResult
import timber.log.Timber
import java.net.ConnectException
import java.net.UnknownHostException
import java.util.concurrent.TimeUnit

/**
 * Created by ZhenisMadiyar on 23,Февраль,2020
 */
class CurrencyListFragment : MvvmFragment(), CurrencyListAdapter.InterfaceCommunicator {

    private val currencyViewModel = MyApp.injectCurrencyViewModel()
    private var currencyAdapter: CurrencyListAdapter? = null
    private var selectedCurrency: String = "EUR" //default value is EUR

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_currency_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        initRV()
    }

    private fun initRV() {
        currencyAdapter = CurrencyListAdapter(activity!!)
        rv_currencies.layoutManager = LinearLayoutManager(activity)
        rv_currencies.adapter = currencyAdapter
        currencyAdapter?.setOnItemClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        subscribe(
            Observable.interval(1, TimeUnit.SECONDS)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    currencyViewModel.getCurrencies(selectedCurrency)
                        .subscribeOn(Schedulers.io())
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe({
                            Timber.d("Received UIModel $it currency result.")
                            showCurrencyList(it)
                        }, {
                            Timber.w(it)
                            showError()
                        })
                }, {
                    Timber.e(it)
                })
        )
    }

    override fun onItemSelected(item: RateItem) {
        currencyAdapter?.changePosition(item)
        selectedCurrency = item.name
        rv_currencies.smoothScrollToPosition(0)
    }

    private fun showCurrencyList(data: CurrencyResult) {
        if (data.error == null) {
            currencyAdapter?.loadList(data.rateList)
        } else if (data.error is ConnectException || data.error is UnknownHostException) {
            Timber.d("No connection, please try later!")
        } else {
            showError()
        }
    }

    private fun showError() {
        Toast.makeText(context, "An error occurred :(", Toast.LENGTH_SHORT).show()
    }
}